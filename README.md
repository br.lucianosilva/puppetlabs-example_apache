# example_apache

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with example_apache](#setup)
    * [Beginning with example_apache](#beginning-with-example_apache)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description
This is one example Module of Apache for Puppet.
In this Module I define Package, Service, Files of Apache and one Index.html Template.
Based on test for dicactic purposes.

## setup
### beginning-with-example_apache
To use this module you should only have the Puppe installed.
Optionally you can use it with the R10K.

## Usage

This is a Structure of Module:

*  |-- examples 
*  |   `-- config.pp 
*  |-- files 
*  |   `-- index.html
*  |-- Gemfile
*  |-- manifests
*  |   |-- config.pp
*  |   |-- params.pp
*  |   `-- vhost.pp
*  |-- metadata.json
*  |-- Rakefile
*  |-- README.md
*  |-- spec
*  |   |-- classes
*  |   |   `-- init_spec.rb
*  |   `-- spec_helper.rb
*  `-- templates
*     |-- bkp-index.html.erb
*     |-- index.html.erb
*     `-- vhost.conf.erb


## Reference

* Docs: https://docs.puppet.com/
* Resources: https://puppet.com/docs/puppet/5.3/type.html

## Limitations

List OS compatibility:
* CentOS
I just tested this Module in a CentOS 7 Instance.

## Development

Me and God hehehehe.


