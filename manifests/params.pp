# --------------------
# Class Params
# --------------------
class example_apache::params {
	# Conditional structures for OS[family] Factor
	case $::os['family'] {
	 'RedHat': {
		# Define variables
		$init_text = "Welcome"
		$httpd_user = 'apache'
		$httpd_group = 'apache'
		$httpd_pkg = 'httpd'
		$httpd_svc = 'httpd'
		$httpd_docroot = '/var/www/html'		
	 }
	 'Debian': { 
		# Define variables
		$init_text = "Welcome :C Grrrrr"
		$httpd_user = 'www-data'
		$httpd_group = 'www-data'
		$httpd_pkg = 'apache2'
		$httpd_svc = 'apache2'
		$httpd_docroot = '/var/www'		
	 }
	 default: {
		# Default message
	 	fail("Module ${module_name} is not supported on ${::osfamily}")
	 }
	}
}
