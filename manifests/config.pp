# ---------------------
# 	Class Principal
# ---------------------
class example_apache::config (
# --------------------
# Variaveis
# --------------------
$init_text = $example_apache::params::init_text,
$httpd_user = $example_apache::params::httpd_user,
$httpd_group = $example_apache::params::httpd_group,
$httpd_pkg = $example_apache::params::httpd_pkg,
$httpd_svc = $example_apache::params::httpd_svc,
$httpd_docroot = $example_apache::params::httpd_docroot,
) inherits example_apache::params {
# -----------------
# Resources
# -----------------
	package {$httpd_pkg:
		ensure => present,
	}
	# Default Resource File
	File { 
		owner => $httpd_user,
		group => $httpd_group,
		mode => "0644",
	}
	# Resource File for Index.html 
	file {"$httpd_docroot/index.html":
		ensure => file,
		content => template('example_apache/index.html.erb'),
		require => Package[$httpd_pkg],
	}
	# Resource Service for status of service
	service {$httpd_pkg:
		ensure => running,
		enable => true,
		subscribe => File["$httpd_docroot/index.html"],
	}
	notify {'Mensagem':
		message =>"Manifest aplicado com sucesso!!!",
	}
	
	#example_apache::vhost {'puppet-server01.local':
	#	port => '80',
	#	docroot => '/var/www/sesame/puppet-server01',
	#	options => 'Indexes MultiViews'
	#}
}
