define example_apache::vhost (
	$docroot,
	$port = '80',
	$priority = '10',
	$options = 'Indexes MultiViews',
	$vhost_name = $title,
	$servename = $title,
	$logdir = '/var/log/httpd',
){
	File {
		owner => "apache",
		group => "apache",
		mode => "0644",
	}
	file {"/etc/httpd/conf.d/${title}.conf":
		ensure => file,
		content => template('example_apache/vhost.conf.erb'),
		notify => Service['httpd'],
	}
}
